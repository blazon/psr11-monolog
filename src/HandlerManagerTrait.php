<?php

namespace Blazon\PSR11MonoLog;

use Blazon\PSR11MonoLog\Exception\MissingServiceException;
use Blazon\PSR11MonoLog\Service\HandlerManager;

trait HandlerManagerTrait
{
    /** @var HandlerManager */
    protected $handlerManager;

    public function getHandlerManager(): HandlerManager
    {
        /** @phpstan-ignore-next-line */
        if (!$this->handlerManager) {
            throw new MissingServiceException("Handler Manager service not set");
        }

        return $this->handlerManager;
    }

    public function setHandlerManager(HandlerManager $handlerManager)
    {
        $this->handlerManager = $handlerManager;
    }
}
