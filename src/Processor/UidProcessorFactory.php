<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\UidProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class UidProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): UidProcessor
    {
        $length = (int) ($options['length'] ?? 7);
        return new UidProcessor($length);
    }
}
