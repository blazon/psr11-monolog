<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\TagProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class TagProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): TagProcessor
    {
        $tags = (array) ($options['tags'] ?? []);
        return new TagProcessor($tags);
    }
}
