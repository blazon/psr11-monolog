<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Logger;
use Monolog\Processor\MercurialProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class MercurialProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): MercurialProcessor
    {
        $level = $options['level'] ?? Logger::DEBUG;
        return new MercurialProcessor($level);
    }
}
