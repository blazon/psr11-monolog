<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\MemoryUsageProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class MemoryUsageProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): MemoryUsageProcessor
    {
        return new MemoryUsageProcessor();
    }
}
