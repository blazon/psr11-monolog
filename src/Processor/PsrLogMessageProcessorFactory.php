<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\PsrLogMessageProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class PsrLogMessageProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): PsrLogMessageProcessor
    {
        return new PsrLogMessageProcessor();
    }
}
