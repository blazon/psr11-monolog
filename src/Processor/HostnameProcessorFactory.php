<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\HostnameProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class HostnameProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): HostnameProcessor
    {
        return new HostnameProcessor();
    }
}
