<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Logger;
use Monolog\Processor\GitProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class GitProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): GitProcessor
    {
        $level = $options['level'] ?? Logger::DEBUG;
        return new GitProcessor($level);
    }
}
