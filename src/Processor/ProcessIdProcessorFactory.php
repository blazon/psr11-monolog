<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\ProcessIdProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class ProcessIdProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): ProcessIdProcessor
    {
        return new ProcessIdProcessor();
    }
}
