<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Monolog\Processor\MemoryPeakUsageProcessor;
use Blazon\PSR11MonoLog\FactoryInterface;

class MemoryPeakUsageProcessorFactory implements FactoryInterface
{
    public function __invoke(array $options): MemoryPeakUsageProcessor
    {
        return new MemoryPeakUsageProcessor();
    }
}
