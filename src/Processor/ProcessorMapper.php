<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use Blazon\PSR11MonoLog\MapperInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProcessorMapper implements MapperInterface
{
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function map(string $type): ?string
    {
        $type = strtolower($type);

        switch ($type) {
            case 'psrlogmessage':
                return PsrLogMessageProcessorFactory::class;
            case 'introspection':
                return IntrospectionProcessorFactory::class;
            case 'web':
                return WebProcessorFactory::class;
            case 'memoryusage':
                return MemoryUsageProcessorFactory::class;
            case 'memorypeak':
                return MemoryPeakUsageProcessorFactory::class;
            case 'processid':
                return ProcessIdProcessorFactory::class;
            case 'uid':
                return UidProcessorFactory::class;
            case 'git':
                return GitProcessorFactory::class;
            case 'mercurial':
                return MercurialProcessorFactory::class;
            case 'tags':
                return TagProcessorFactory::class;
            case 'hostname':
                return HostnameProcessorFactory::class;
        }

        return null;
    }
}
