<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Processor;

use ArrayAccess;
use Monolog\Processor\WebProcessor;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\ContainerTrait;
use Blazon\PSR11MonoLog\Exception\MissingServiceException;
use Blazon\PSR11MonoLog\FactoryInterface;

class WebProcessorFactory implements FactoryInterface, ContainerAwareInterface
{
    use ContainerTrait;

    public function __invoke(array $options): WebProcessor
    {
        $serverData  = $this->getServerDataService($options);
        $extraFields = $options['extraFields'] ?? null;

        return new WebProcessor(
            $serverData,
            $extraFields
        );
    }

    public function getServerDataService(array $options)
    {
        if (empty($options['serverData'])) {
            return null;
        }

        if (
            is_array($options['serverData'])
            || $options['serverData'] instanceof ArrayAccess
        ) {
            return $options['serverData'];
        }

        if (!$this->getContainer()->has($options['serverData'])) {
            throw new MissingServiceException(
                'No serverData service found'
            );
        }

        return $this->getContainer()->get($options['serverData']);
    }
}
