<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\BufferHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\HandlerManagerAwareInterface;
use Blazon\PSR11MonoLog\HandlerManagerTrait;
use Blazon\PSR11MonoLog\FactoryInterface;

class BufferHandlerFactory implements FactoryInterface, HandlerManagerAwareInterface
{
    use HandlerManagerTrait;

    public function __invoke(array $options): BufferHandler
    {
        $handler          = $this->getHandlerManager()->get($options['handler']);
        $bufferLimit      = (int)     ($options['bufferLimit']     ?? 0);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble           = (bool) ($options['bubble']          ?? true);
        $flushOnOverflow  = (bool) ($options['flushOnOverflow'] ?? true);

        return new BufferHandler(
            $handler,
            $bufferLimit,
            $level,
            $bubble,
            $flushOnOverflow
        );
    }
}
