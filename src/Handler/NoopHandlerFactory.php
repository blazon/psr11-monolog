<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\NoopHandler;
use Blazon\PSR11MonoLog\FactoryInterface;

class NoopHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): NoopHandler
    {
        return new NoopHandler();
    }
}
