<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FilterHandler;
use Blazon\PSR11MonoLog\HandlerManagerAwareInterface;
use Blazon\PSR11MonoLog\HandlerManagerTrait;
use Blazon\PSR11MonoLog\FactoryInterface;
use Monolog\Logger;

class FilterHandlerFactory implements FactoryInterface, HandlerManagerAwareInterface
{
    use HandlerManagerTrait;

    public function __invoke(array $options): FilterHandler
    {
        $handler = $this->getHandlerManager()->get($options['handler']);
        $minLevelOrList = $options['minLevelOrList'] ?? Logger::DEBUG;
        $maxLevel = $options['maxLevel'] ?? Logger::EMERGENCY;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new FilterHandler(
            $handler,
            $minLevelOrList,
            $maxLevel,
            $bubble
        );
    }
}
