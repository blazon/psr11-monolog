<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\SendGridHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class SendGridHandlerFactory implements FactoryInterface
{
    /**
     * * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function __invoke(array $options): SendGridHandler
    {
        $apiUser = (string) ($options['apiUser'] ?? '');
        $apiKey = (string) ($options['apiKey'] ?? '');
        $from = (string) ($options['from'] ?? '');
        $to = $options['to'] ?? '';
        $subject = (string) ($options['subject'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new SendGridHandler(
            $apiUser,
            $apiKey,
            $from,
            $to,
            $subject,
            $level,
            $bubble
        );
    }
}
