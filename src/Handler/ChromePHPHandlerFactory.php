<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\ChromePHPHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class ChromePHPHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): ChromePHPHandler
    {
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new ChromePHPHandler(
            $level,
            $bubble
        );
    }
}
