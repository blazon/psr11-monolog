<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FleepHookHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class FleepHookHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): FleepHookHandler
    {
        $token = (string) ($options['token'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new FleepHookHandler(
            $token,
            $level,
            $bubble
        );
    }
}
