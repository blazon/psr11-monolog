<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\SqsHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class SqsHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): SqsHandler
    {
        $sqsClient = $this->getService($options['sqsClient'] ?? null);

        $queueUrl = (string) $options['queueUrl'] ?? '';
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new SqsHandler($sqsClient, $queueUrl, $level, $bubble);
    }
}
