<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\PHPConsoleHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class PHPConsoleHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): PHPConsoleHandler
    {
        $consoleOptions = (array)   ($options['options'] ?? []);
        $connector = $this->getService($options['connector'] ?? null);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble']  ?? true);

        return new PHPConsoleHandler(
            $consoleOptions,
            $connector,
            $level,
            $bubble
        );
    }
}
