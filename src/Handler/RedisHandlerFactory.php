<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\RedisHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class RedisHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): RedisHandler
    {
        $client = $this->getService($options['client'] ?? []);
        $key = (string) ($options['key'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);
        $capSize = (int) ($options['capSize'] ?? 0);

        return new RedisHandler(
            $client,
            $key,
            $level,
            $bubble,
            $capSize
        );
    }
}
