<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\DoctrineCouchDBHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class DoctrineCouchDBHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): DoctrineCouchDBHandler
    {
        $client = $this->getService($options['client'] ?? null);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new DoctrineCouchDBHandler(
            $client,
            $level,
            $bubble
        );
    }
}
