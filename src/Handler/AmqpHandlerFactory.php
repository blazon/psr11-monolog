<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\AmqpHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class AmqpHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): AmqpHandler
    {
        $exchange = $this->getService($options['exchange'] ?? null);
        $exchangeName = (string) ($options['exchangeName'] ?? 'log');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new AmqpHandler(
            $exchange,
            $exchangeName,
            $level,
            $bubble
        );
    }
}
