<?php

namespace Blazon\PSR11MonoLog\Handler;

use Blazon\PSR11MonoLog\Exception\MissingConfigException;
use Blazon\PSR11MonoLog\Exception\MissingServiceException;
use Blazon\PSR11MonoLog\ServiceTrait;
use Swift_Message;

trait SwiftMessageTrait
{
    use ServiceTrait;

    /**
     * @return callable|Swift_Message
     */
    public function getSwiftMessage(array $options)
    {
        if (empty($options['message'])) {
            throw new MissingConfigException(
                'No message service name or callback provided'
            );
        }

        if (is_callable($options['message'])) {
            return $options['message'];
        }

        if (!$this->getContainer()->has($options['message'])) {
            throw new MissingServiceException(
                'No Message service found'
            );
        }

        return $this->getContainer()->get($options['message']);
    }
}
