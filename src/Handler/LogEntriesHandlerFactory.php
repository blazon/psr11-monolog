<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\LogEntriesHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class LogEntriesHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): LogEntriesHandler
    {
        $token = (string) ($options['token'] ?? '');
        $useSSL = (bool) ($options['useSSL'] ?? true);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new LogEntriesHandler(
            $token,
            $useSSL,
            $level,
            $bubble
        );
    }
}
