<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\IFTTTHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class IFTTTHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): IFTTTHandler
    {
        $eventName = (string) ($options['eventName'] ?? '');
        $secretKey = (string) ($options['secretKey'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new IFTTTHandler(
            $eventName,
            $secretKey,
            $level,
            $bubble
        );
    }
}
