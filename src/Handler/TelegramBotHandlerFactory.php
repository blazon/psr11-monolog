<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\TelegramBotHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class TelegramBotHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): TelegramBotHandler
    {
        $apiKey = (string) ($options['apiKey'] ?? '');
        $channel = (string) ($options['channel'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new TelegramBotHandler(
            $apiKey,
            $channel,
            $level,
            $bubble
        );
    }
}
