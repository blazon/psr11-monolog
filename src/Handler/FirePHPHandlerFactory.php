<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FirePHPHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class FirePHPHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): FirePHPHandler
    {
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new FirePHPHandler(
            $level,
            $bubble
        );
    }
}
