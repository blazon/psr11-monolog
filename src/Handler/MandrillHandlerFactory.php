<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\MandrillHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;

class MandrillHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use SwiftMessageTrait;

    public function __invoke(array $options): MandrillHandler
    {
        $apiKey  = (string) ($options['apiKey'] ?? '');
        $message = $this->getSwiftMessage($options);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new MandrillHandler(
            $apiKey,
            $message,
            $level,
            $bubble
        );
    }
}
