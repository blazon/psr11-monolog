<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\ContainerTrait;
use Blazon\PSR11MonoLog\FactoryInterface;

class StreamHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ContainerTrait;

    /** @var ContainerInterface */
    protected $container;

    /**
     * @param array $options
     * @return StreamHandler
     * @throws Exception
     */
    public function __invoke(array $options): StreamHandler
    {
        $stream = $this->getStream($options['stream'] ?? null);

        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);
        $filePermission = (int) ($options['filePermission'] ?? 0644);
        $useLocking = (bool) ($options['useLocking'] ?? true);

        return new StreamHandler($stream, $level, $bubble, $filePermission, $useLocking);
    }

    /** @return resource|string */
    protected function getStream($stream)
    {
        if ($this->container->has((string) $stream)) {
            return $this->container->get($stream);
        }

        if (is_resource($stream)) {
            return $stream;
        }

        return (string) $stream;
    }
}
