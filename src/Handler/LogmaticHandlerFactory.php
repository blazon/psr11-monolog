<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\LogmaticHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class LogmaticHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): LogmaticHandler
    {
        $token = (string) ($options['token'] ?? '');
        $hostname = (string) ($options['hostname'] ?? '');
        $appname = (string) ($options['appname'] ?? '');
        $useSSL = (bool) ($options['useSSL'] ?? true);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new LogmaticHandler(
            $token,
            $hostname,
            $appname,
            $useSSL,
            $level,
            $bubble
        );
    }
}
