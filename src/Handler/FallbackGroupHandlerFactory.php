<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FallbackGroupHandler;

class FallbackGroupHandlerFactory
{
    use GetHandlersTrait;

    public function __invoke(array $options): FallbackGroupHandler
    {
        $handlers = $this->getHandlers($options);
        $bubble   = (bool) ($options['bubble'] ?? true);

        return new FallbackGroupHandler(
            $handlers,
            $bubble
        );
    }
}
