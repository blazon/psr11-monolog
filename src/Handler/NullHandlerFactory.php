<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\NullHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class NullHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): NullHandler
    {
        $level = $options['level'] ?? Logger::DEBUG;

        return new NullHandler($level);
    }
}
