<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FlowdockHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class FlowdockHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): FlowdockHandler
    {
        $apiToken = (string) ($options['apiToken'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new FlowdockHandler(
            $apiToken,
            $level,
            $bubble
        );
    }
}
