<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class BrowserConsoleHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): BrowserConsoleHandler
    {
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new BrowserConsoleHandler(
            $level,
            $bubble
        );
    }
}
