<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\PushoverHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class PushoverHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): PushoverHandler
    {
        $token = (string) ($options['token'] ?? '');
        $users = (array) ($options['users'] ?? []);
        $title = $options['title'] ?? null;
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);
        $useSSL = (bool) ($options['useSSL'] ?? true);
        $highPriorityLevel = $options['highPriorityLevel'] ?? Logger::CRITICAL;
        $emergencyLevel = $options['emergencyLevel'] ?? Logger::EMERGENCY;
        $retry = (int) ($options['retry'] ?? 30);
        $expire = (int) ($options['expire'] ?? 25200);

        return new PushoverHandler(
            $token,
            $users,
            $title,
            $level,
            $bubble,
            $useSSL,
            $highPriorityLevel,
            $emergencyLevel,
            $retry,
            $expire
        );
    }
}
