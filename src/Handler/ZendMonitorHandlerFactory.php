<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\MissingExtensionException;
use Monolog\Handler\ZendMonitorHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

/**
 * @codeCoverageIgnore
 *
 * No Zend Server available to test against
 */
class ZendMonitorHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): ZendMonitorHandler
    {
        $level = $options['level']  ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new ZendMonitorHandler(
            $level,
            $bubble
        );
    }
}
