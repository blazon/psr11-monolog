<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\DynamoDbHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class DynamoDbHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): DynamoDbHandler
    {
        $client = $this->getService($options['client'] ?? null);
        $table = (string) ($options['table'] ?? null);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new DynamoDbHandler(
            $client,
            $table,
            $level,
            $bubble
        );
    }
}
