<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\InsightOpsHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class InsightOpsHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): InsightOpsHandler
    {
        $token  = (string) ($options['token']  ?? '');
        $region = (string) ($options['region'] ?? '');
        $useSSL = (bool) ($options['useSSL'] ?? true);
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new InsightOpsHandler(
            $token,
            $region,
            $useSSL,
            $level,
            $bubble
        );
    }
}
