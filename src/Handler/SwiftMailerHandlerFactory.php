<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\SwiftMailerHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;

class SwiftMailerHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use SwiftMessageTrait;

    public function __invoke(array $options): SwiftMailerHandler
    {
        $mailer = $this->getService($options['mailer'] ?? null);
        $message = $this->getSwiftMessage($options);

        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new SwiftMailerHandler($mailer, $message, $level, $bubble);
    }
}
