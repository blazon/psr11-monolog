<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\MongoDBHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\ServiceTrait;

class MongoDBHandlerFactory implements FactoryInterface, ContainerAwareInterface
{
    use ServiceTrait;

    public function __invoke(array $options): MongoDBHandler
    {
        $client = $this->getService($options['client'] ?? null);
        $database = (string)  ($options['database'] ?? '');
        $collection = (string)  ($options['collection'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new MongoDBHandler(
            $client,
            $database,
            $collection,
            $level,
            $bubble
        );
    }
}
