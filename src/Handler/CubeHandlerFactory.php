<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\CubeHandler;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class CubeHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): CubeHandler
    {
        $url = (string) ($options['url'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new CubeHandler(
            $url,
            $level,
            $bubble
        );
    }
}
