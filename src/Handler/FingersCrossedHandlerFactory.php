<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\FingersCrossedHandler;
use Blazon\PSR11MonoLog\HandlerManagerAwareInterface;
use Blazon\PSR11MonoLog\HandlerManagerTrait;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\ContainerTrait;
use Blazon\PSR11MonoLog\FactoryInterface;

class FingersCrossedHandlerFactory implements FactoryInterface, HandlerManagerAwareInterface, ContainerAwareInterface
{
    use ContainerTrait;
    use HandlerManagerTrait;

    public function __invoke(array $options): FingersCrossedHandler
    {
        $handler = $this->getHandlerManager()->get($options['handler']);
        $activationStrategy = $this->getActivationStrategy($options);
        $bufferSize = (int) ($options['bufferSize'] ?? 0);
        $bubble = (bool) ($options['bubble'] ?? true);
        $stopBuffering = (bool) ($options['stopBuffering'] ?? true);
        $passthruLevel = $options['passthruLevel'] ?? null;

        return new FingersCrossedHandler(
            $handler,
            $activationStrategy,
            $bufferSize,
            $bubble,
            $stopBuffering,
            $passthruLevel
        );
    }

    protected function getActivationStrategy($options)
    {
        $activationStrategy = $options['activationStrategy'] ?? null;

        if (!$activationStrategy) {
            return null;
        }

        if ($this->getContainer()->has((string) $activationStrategy)) {
            return $this->getContainer()->get($activationStrategy);
        }

        return $activationStrategy;
    }
}
