<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\GroupHandler;
use Blazon\PSR11MonoLog\Exception\MissingConfigException;
use Blazon\PSR11MonoLog\HandlerManagerAwareInterface;
use Blazon\PSR11MonoLog\HandlerManagerTrait;
use Blazon\PSR11MonoLog\FactoryInterface;

class GroupHandlerFactory implements FactoryInterface, HandlerManagerAwareInterface
{
    use GetHandlersTrait;

    public function __invoke(array $options): GroupHandler
    {
        $handlers = $this->getHandlers($options);
        $bubble   = (bool) ($options['bubble'] ?? true);

        return new GroupHandler(
            $handlers,
            $bubble
        );
    }
}
