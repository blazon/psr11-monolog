<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Blazon\PSR11MonoLog\Exception\MissingConfigException;
use Blazon\PSR11MonoLog\HandlerManagerTrait;

trait GetHandlersTrait
{
    use HandlerManagerTrait;

    protected function getHandlers($options): array
    {
        $handlers = $options['handlers'] ?? [];

        if (empty($handlers) || !is_array($handlers)) {
            throw new MissingConfigException(
                "No handlers specified"
            );
        }

        $return = [];

        foreach ($handlers as $handler) {
            $return[] = $this->getHandlerManager()->get($handler);
        }

        return $return;
    }
}
