<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\LogglyHandler;
use Monolog\Handler\MissingExtensionException;
use Monolog\Logger;
use Blazon\PSR11MonoLog\FactoryInterface;

class LogglyHandlerFactory implements FactoryInterface
{
    public function __invoke(array $options): LogglyHandler
    {
        $token = (string) ($options['token'] ?? '');
        $level = $options['level'] ?? Logger::DEBUG;
        $bubble = (bool) ($options['bubble'] ?? true);

        return new LogglyHandler(
            $token,
            $level,
            $bubble
        );
    }
}
