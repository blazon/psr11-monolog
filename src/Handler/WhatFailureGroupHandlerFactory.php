<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Handler;

use Monolog\Handler\WhatFailureGroupHandler;

class WhatFailureGroupHandlerFactory
{
    use GetHandlersTrait;

    public function __invoke(array $options): WhatFailureGroupHandler
    {
        $handlers = $this->getHandlers($options);
        $bubble = (bool) ($options['bubble'] ?? true);

        return new WhatFailureGroupHandler(
            $handlers,
            $bubble
        );
    }
}
