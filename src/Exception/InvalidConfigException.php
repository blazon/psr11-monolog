<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Exception;

use InvalidArgumentException;

class InvalidConfigException extends InvalidArgumentException
{
}
