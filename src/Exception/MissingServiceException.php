<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Exception;

use OutOfBoundsException;

class MissingServiceException extends OutOfBoundsException
{
}
