<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Exception;

use Psr\Container\NotFoundExceptionInterface;
use OutOfBoundsException;

class UnknownServiceException extends OutOfBoundsException implements NotFoundExceptionInterface
{
}
