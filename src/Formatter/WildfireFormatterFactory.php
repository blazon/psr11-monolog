<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\WildfireFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class WildfireFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): WildfireFormatter
    {
        $dateFormat = $options['dateFormat'] ?? null;
        return new WildfireFormatter($dateFormat);
    }
}
