<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\ScalarFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class ScalarFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): ScalarFormatter
    {
        return new ScalarFormatter();
    }
}
