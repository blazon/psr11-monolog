<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\MongoDBFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

/**
 * @SuppressWarnings("LongVariable")
 */
class MongoDBFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): MongoDBFormatter
    {
        $maxNestingLevel = (int) ($options['maxNestingLevel'] ?? 3);
        $exceptionTraceAsString = (bool) ($options['exceptionTraceAsString'] ?? true);
        return new MongoDBFormatter($maxNestingLevel, $exceptionTraceAsString);
    }
}
