<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\ChromePHPFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class ChromePHPFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): ChromePHPFormatter
    {
        return new ChromePHPFormatter();
    }
}
