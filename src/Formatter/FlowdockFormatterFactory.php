<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\FlowdockFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class FlowdockFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): FlowdockFormatter
    {
        $source = (string) ($options['source'] ?? null);
        $sourceEmail = (string) ($options['sourceEmail'] ?? null);
        return new FlowdockFormatter($source, $sourceEmail);
    }
}
