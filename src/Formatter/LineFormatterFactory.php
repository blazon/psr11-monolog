<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\LineFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

/**
 * @SuppressWarnings("LongVariable")
 */
class LineFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): LineFormatter
    {
        $format = $options['format'] ?? null;
        $dateFormat = $options['dateFormat'] ?? null;
        $allowInlineLineBreaks = (bool) ($options['allowInlineLineBreaks'] ?? false);
        $ignoreEmptyContextAndExtra = (bool) ($options['ignoreEmptyContextAndExtra'] ?? false);

        return new LineFormatter(
            $format,
            $dateFormat,
            $allowInlineLineBreaks,
            $ignoreEmptyContextAndExtra
        );
    }
}
