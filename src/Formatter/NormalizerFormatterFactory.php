<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\NormalizerFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class NormalizerFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): NormalizerFormatter
    {
        $dateFormat = $options['dateFormat'] ?? null;
        return new NormalizerFormatter($dateFormat);
    }
}
