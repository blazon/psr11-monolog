<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Formatter;

use Monolog\Formatter\HtmlFormatter;
use Blazon\PSR11MonoLog\FactoryInterface;

class HtmlFormatterFactory implements FactoryInterface
{
    public function __invoke(array $options): HtmlFormatter
    {
        $dateFormat = $options['dateFormat'] ?? null;
        return new HtmlFormatter($dateFormat);
    }
}
