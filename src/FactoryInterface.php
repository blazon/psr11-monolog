<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog;

interface FactoryInterface
{
    public function __invoke(array $options);
}
