<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Config;

use Blazon\PSR11MonoLog\ConfigInterface;
use Blazon\PSR11MonoLog\Exception\MissingConfigException;

abstract class AbstractServiceConfig implements ConfigInterface
{
    protected $config = [];

    public function __construct(array $config)
    {
        $this->validateConfig($config);
        $this->config = $config;
    }

    protected function validateConfig($config): void
    {
        if (empty($config)) {
            throw new MissingConfigException(
                'No config found'
            );
        }

        if (empty($config['type'])) {
            throw new MissingConfigException(
                'No config key of "type" found in adaptor config array.'
            );
        }
    }

    public function getType(): string
    {
        return $this->config['type'];
    }

    public function getOptions(): array
    {
        return $this->config['options'] ?? [];
    }
}
