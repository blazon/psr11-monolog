<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Config;

class HandlerConfig extends AbstractServiceConfig
{
    public function getFormatter(): string
    {
        return $this->config['formatter'] ?? '';
    }

    public function getProcessors(): array
    {
        return $this->config['processors'] ?? [];
    }
}
