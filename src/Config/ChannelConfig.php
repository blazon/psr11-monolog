<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Config;

use Blazon\PSR11MonoLog\Exception\MissingConfigException;

class ChannelConfig
{
    protected $config = [];

    public function __construct(array $config)
    {
        $this->validateConfig($config);
        $this->config = $config;
    }

    protected function validateConfig($config): void
    {
        if (empty($config)) {
            throw new MissingConfigException(
                'No config found'
            );
        }

        if (empty($config['handlers'])) {
            throw new MissingConfigException(
                'No config key of "handlers" found in channel config array.'
            );
        }
    }

    public function getHandlers(): array
    {
        return $this->config['handlers'];
    }

    public function getProcessors(): array
    {
        return $this->config['processors'] ?? [];
    }

    public function getName(): ?string
    {
        return $this->config['name'] ?? null;
    }
}
