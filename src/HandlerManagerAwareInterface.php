<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog;

use Blazon\PSR11MonoLog\Service\HandlerManager;

interface HandlerManagerAwareInterface
{
    public function getHandlerManager(): HandlerManager;
    public function setHandlerManager(HandlerManager $container);
}
