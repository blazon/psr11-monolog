<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog;

interface MapperInterface
{
    public function map(string $type);
}
