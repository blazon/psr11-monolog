<?php

namespace Blazon\PSR11MonoLog\Service;

use Blazon\PSR11MonoLog\Exception\MissingConfigException;
use Psr\Container\ContainerInterface;
use Blazon\PSR11MonoLog\Config\MainConfig;
use Blazon\PSR11MonoLog\ConfigInterface;
use Blazon\PSR11MonoLog\ContainerAwareInterface;
use Blazon\PSR11MonoLog\Exception\InvalidConfigException;
use Blazon\PSR11MonoLog\Exception\UnknownServiceException;
use Blazon\PSR11MonoLog\FactoryInterface;
use Blazon\PSR11MonoLog\HandlerManagerAwareInterface;
use Blazon\PSR11MonoLog\MapperInterface;

abstract class AbstractServiceManager implements ContainerInterface
{
    /** @var MainConfig */
    protected $config;

    /** @var MapperInterface */
    protected $mapper;

    /** @var ContainerInterface */
    protected $container;

    /** @var array */
    protected $services = [];

    public function __construct(
        MainConfig $config,
        MapperInterface $mapper,
        ContainerInterface $container
    ) {
        $this->config = $config;
        $this->mapper = $mapper;
        $this->container = $container;
    }

    abstract protected function getServiceConfig($id): ?ConfigInterface;
    abstract protected function hasServiceConfig($id): bool;

    public function get($id)
    {
        if (key_exists($id, $this->services)) {
            return $this->services[$id];
        }

        // Make sure we have one of these
        if (!$this->has($id)) {
            throw new UnknownServiceException(
                'Unable to locate service ' . $id . '.  Please check your configuration.'
            );
        }

        // Check the main container for this service id.
        if ($this->container->has($id)) {
            $this->services[$id] = $this->container->get($id);
            return $this->services[$id];
        }

        $this->services[$id] = $this->getInstanceFromFactory($id);
        return $this->services[$id];
    }

    public function has($id): bool
    {
        if (key_exists($id, $this->services)) {
            return true;
        }

        if ($this->container->has($id)) {
            return true;
        }

        return $this->hasServiceConfig($id);
    }

    protected function getInstanceFromFactory($id)
    {
        $class   = null;

        $config  = $this->getServiceConfig($id);

        if (!$config) {
            throw new MissingConfigException(
                'Unable to find service config'
            );
        }

        $type    = $config->getType();
        $options = $config->getOptions();

        $class = $type;

        // Check for class and class implements of Monolog Formatter Interface
        if (
            !class_exists($class)
            || !in_array(FactoryInterface::class, class_implements($class))
        ) {
            $class = $this->mapper->map($type);
        }

        if (
            !class_exists($class)
            || !in_array(FactoryInterface::class, class_implements($class))
        ) {
            throw new InvalidConfigException(
                $id . '.  Is not a valid factory.  Please check your configuration.'
            );
        }

        /** @var FactoryInterface $factory */
        $factory = new $class();

        if ($factory instanceof ContainerAwareInterface) {
            $factory->setContainer($this->container);
        }

        // @codeCoverageIgnoreStart
        if (
            $factory instanceof HandlerManagerAwareInterface
            && $this instanceof HandlerManager
        ) {
            $factory->setHandlerManager($this);
        }
        // @codeCoverageIgnoreEnd

        return $factory($options);
    }
}
