<?php

namespace Blazon\PSR11MonoLog\Service;

use Monolog\Formatter\FormatterInterface;
use Monolog\Handler\HandlerInterface;
use Blazon\PSR11MonoLog\ConfigInterface;
use Blazon\PSR11MonoLog\Exception\MissingServiceException;
use Blazon\PSR11MonoLog\Exception\UnknownServiceException;

class HandlerManager extends AbstractServiceManager
{
    /** @var FormatterManager */
    protected $formatterManager;

    /**
     * @var ProcessorManager
     */
    protected $processorManager;

    public function getServiceConfig($id): ?ConfigInterface
    {
        return $this->config->getHandlerConfig($id);
    }

    public function hasServiceConfig($id): bool
    {
        return $this->config->hasHandlerConfig($id);
    }

    public function get($id): HandlerInterface
    {
        if (array_key_exists($id, $this->services)) {
            return $this->services[$id];
        }

        /** @var HandlerInterface $handler */
        $handler = parent::get($id);

        $config = $this->config->getHandlerConfig($id);

        if (!$config) {
            return $handler;
        }

        $formatter = $config->getFormatter();

        if ($formatter && method_exists($handler, 'setFormatter')) {
            $handler->setFormatter($this->getFormatter($formatter));
        }

        $processors = $config->getProcessors();

        if (!method_exists($handler, 'pushProcessor')) {
            return $handler;
        }

        foreach ($processors as $processorName) {
            $handler->pushProcessor($this->getProcessorManager()->get($processorName));
        }

        return $handler;
    }

    public function setFormatterManager(FormatterManager $manager)
    {
        $this->formatterManager = $manager;
    }

    public function getFormatterManager(): FormatterManager
    {
        /** @phpstan-ignore-next-line */
        if (!$this->formatterManager) {
            throw new MissingServiceException(
                'Unable to get FormatterManager.'
            );
        }

        return $this->formatterManager;
    }

    protected function getFormatter($id): FormatterInterface
    {
        if (!$this->getFormatterManager()->has($id)) {
            throw new UnknownServiceException(
                'Unable to locate formatter ' . $id
            );
        }

        return $this->getFormatterManager()->get($id);
    }

    public function setProcessorManager(ProcessorManager $processorManager)
    {
        $this->processorManager = $processorManager;
    }

    public function getProcessorManager(): ProcessorManager
    {
        /** @phpstan-ignore-next-line */
        if (!$this->processorManager) {
            throw new MissingServiceException(
                'Unable to get ProcessorManager.'
            );
        }

        return $this->processorManager;
    }
}
