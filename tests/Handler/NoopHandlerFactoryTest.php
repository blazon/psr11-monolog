<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Handler\NoopHandler;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Handler\NoopHandlerFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Handler\NoopHandlerFactory
 */
class NoopHandlerFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $factory = new NoopHandlerFactory();
        $handler = $factory([]);

        $this->assertInstanceOf(NoopHandler::class, $handler);
    }
}
