<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Handler\PHPConsoleHandler;
use Monolog\Logger;
use PhpConsole\Connector;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Blazon\PSR11MonoLog\Handler\PHPConsoleHandlerFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Handler\PHPConsoleHandlerFactory
 */
class PHPConsoleHandlerFactoryTest extends TestCase
{
    /** @var PHPConsoleHandlerFactory */
    protected $factory;

    /** @var MockObject|ContainerInterface */
    protected $mockContainer;

    protected function setup(): void
    {
        $this->factory = new PHPConsoleHandlerFactory();
        $this->mockContainer = $this->createMock(ContainerInterface::class);
        $this->factory->setContainer($this->mockContainer);
    }

    public function testInvoke()
    {
        $this->markTestSkipped('Package is broken in PHP 8 and doesn\'t appear to be maintained.  Skipping for now.');

        $options = [
            'options'   => [],
            'connector' => 'my-service',
            'level'     => Logger::INFO,
            'bubble'    => false
        ];

        $mockService = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockContainer->expects($this->once())
            ->method('has')
            ->with($this->equalTo('my-service'))
            ->willReturn(true);

        $this->mockContainer->expects($this->once())
            ->method('get')
            ->with($this->equalTo('my-service'))
            ->willReturn($mockService);

        $handler = $this->factory->__invoke($options);

        $this->assertInstanceOf(PHPConsoleHandler::class, $handler);
    }
}
