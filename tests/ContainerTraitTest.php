<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Blazon\PSR11MonoLog\ContainerTrait;

/**
 * @covers \Blazon\PSR11MonoLog\ContainerTrait
 */
class ContainerTraitTest extends TestCase
{
    public function testGetSetContainer()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);

        /** @var ContainerTrait $trait */
        $trait = $this->getMockForTrait(ContainerTrait::class);
        $trait->setContainer($mockContainer);
        $container = $trait->getContainer();

        $this->assertEquals($mockContainer, $container);
    }
}
