<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Formatter;

use Monolog\Formatter\NormalizerFormatter;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Formatter\NormalizerFormatterFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Formatter\NormalizerFormatterFactory
 */
class NormalizerFormatterFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $options = ['dateFormat' => 'c'];
        $factory = new NormalizerFormatterFactory();
        $formatter = $factory($options);

        $this->assertInstanceOf(NormalizerFormatter::class, $formatter);
    }
}
