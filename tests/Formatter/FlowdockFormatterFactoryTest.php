<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Formatter;

use Monolog\Formatter\FlowdockFormatter;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Formatter\FlowdockFormatterFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Formatter\FlowdockFormatterFactory
 */
class FlowdockFormatterFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $options = [
            'source'      => 'some-source',
            'sourceEmail' => 'some-email',
        ];

        $factory = new FlowdockFormatterFactory();
        $formatter = $factory($options);

        $this->assertInstanceOf(FlowdockFormatter::class, $formatter);
    }
}
