<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Formatter;

use Monolog\Formatter\ScalarFormatter;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Formatter\ScalarFormatterFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Formatter\ScalarFormatterFactory
 */
class ScalarFormatterFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $options = [];
        $factory = new ScalarFormatterFactory();
        $formatter = $factory($options);

        $this->assertInstanceOf(ScalarFormatter::class, $formatter);
    }
}
