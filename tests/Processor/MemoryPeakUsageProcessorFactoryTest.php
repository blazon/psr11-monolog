<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Processor\MemoryPeakUsageProcessor;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Processor\MemoryPeakUsageProcessorFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Processor\MemoryPeakUsageProcessorFactory
 */
class MemoryPeakUsageProcessorFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $factory = new MemoryPeakUsageProcessorFactory();
        $handler = $factory([]);
        $this->assertInstanceOf(MemoryPeakUsageProcessor::class, $handler);
    }
}
