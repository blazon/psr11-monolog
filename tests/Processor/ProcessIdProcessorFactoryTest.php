<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Processor\ProcessIdProcessor;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Processor\ProcessIdProcessorFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Processor\ProcessIdProcessorFactory
 */
class ProcessIdProcessorFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $factory = new ProcessIdProcessorFactory();
        $handler = $factory([]);
        $this->assertInstanceOf(ProcessIdProcessor::class, $handler);
    }
}
