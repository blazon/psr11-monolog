<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Processor\TagProcessor;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Processor\TagProcessorFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Processor\TagProcessorFactory
 */
class TagProcessorFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $options = ['tags' => []];
        $factory = new TagProcessorFactory();
        $handler = $factory($options);
        $this->assertInstanceOf(TagProcessor::class, $handler);
    }
}
