<?php

declare(strict_types=1);

namespace Blazon\PSR11MonoLog\Test\Handler;

use Monolog\Processor\MemoryUsageProcessor;
use PHPUnit\Framework\TestCase;
use Blazon\PSR11MonoLog\Processor\MemoryUsageProcessorFactory;

/**
 * @covers \Blazon\PSR11MonoLog\Processor\MemoryUsageProcessorFactory
 */
class MemoryUsageProcessorFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $factory = new MemoryUsageProcessorFactory();
        $handler = $factory([]);
        $this->assertInstanceOf(MemoryUsageProcessor::class, $handler);
    }
}
